#include "jilu_config.h"
#include "tablatal.c"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* read or write action */
enum { db_read, db_write } db_act;

static int db_action = db_write;

void write_tbtl(FILE *fp) {
  printf("opening tablatal file\n");
  tbtl_db t = open_db(fp);
  printf("t.cols is %ld\n", t.cols);
  printf("t.line_len is %ld\n", t.line_len);
  close_db(t);
}

char *get_default_dir() {
  char *dir = getenv("XDG_DOCUMENTS_DIR");
  if (!(dir)) {
    dir = TBTL_FILE_LOCATION;
  }
  return dir;
}

int main(int argc, char **argv) {
  extern int db_action;

  char *dir;
  char *filename;
  char *filepath;
  FILE *fp;

  if (argc > 1) {
    printf("opening file 1:%s\n", argv[1]);
    fp = fopen(argv[1], "ab+");
    if (fp) {
      filepath = argv[1];
    } else {
      filename = argv[1];
    }
  } else {
    filename = TBTL_DEFAULT_FILENAME;
  }
  if (!fp) {
    dir = get_default_dir();
    strcat(dir, DIR_SEPERATOR);
    filepath = (char *)malloc((strlen(dir) + strlen(filename)) * sizeof(char));
    strcpy(filepath, dir);
    strcat(filepath, filename);
    printf("opening file 2:%s\n", filepath);
    fp = fopen(filepath, "ab+");
    if (!fp) {
      free(filepath);
      filepath = (char *)malloc((strlen(dir) + strlen(TBTL_DEFAULT_FILENAME)) *
                                sizeof(char));
      strcpy(filepath, dir);
      strcat(filepath, TBTL_DEFAULT_FILENAME);
      printf("opening file 3:%s\n", filepath);
      fp = fopen(filepath, "ab+");
      if (!fp) {
        printf("error accessing file: %s\n", filepath);
      }
    }
    free(filepath);
  }
  if (fp) {
    write_tbtl(fp);
    return 0;
  } else {
    return 1;
  }
}
