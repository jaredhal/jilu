#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct tbtl_db {
  size_t cols;
  size_t line_len;
  int *indices;
  char **headers;
} tbtl_db;

size_t num_columns(char *header, size_t limit) {
  size_t cols = 1;
  for (size_t i = 0; header[i] != '\n' && i < limit; i++) {
    if (header[i] != ' ' && header[i - 1] == ' ') {
      cols++;
    }
  }
  return cols;
}

size_t get_line_len(char *header, size_t limit) {
  size_t i;
  for (i = 0; header[i] != 0 && i < limit; i++) {
  }
  return i;
}

void set_indices(int *indices, char *header) {
  int n = 0;
  indices[n] = 0;
  n++;
  for (int i = 0; header[i] != '\0'; i++) {
    if (header[i] != ' ' && i != 0 && header[i - 1] == ' ') {
      indices[n] = i;
      n++;
    }
  }
}

tbtl_db open_db(FILE *fp) {
  tbtl_db t;
  size_t len;
  ssize_t read;
  char *header = 0;
  if (!fp) {
    return t;
  }
  read = getline(&header, &len, fp);
  if (read) {
    t.cols = num_columns(header, len);
    t.line_len = get_line_len(header, len);
    t.indices = malloc(sizeof(int) * t.cols);
    set_indices(t.indices, header);
    t.headers = '\0';
  } else {
    t.cols, t.line_len = 0;
    t.indices = 0;
    t.headers = 0;
  }
  return t;
}

void close_db(tbtl_db t) {
  free(t.indices);
  free(t.headers);
}
